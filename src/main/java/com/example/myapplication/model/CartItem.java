package com.example.myapplication.model;

import androidx.cardview.widget.CardView;

import java.util.Objects;

public class CartItem {
    private Meal meal;
    private Location location;
    private int quantity;

    public CardView getItemCard() {
        return itemCard;
    }

    public void setItemCard(CardView itemCard) {
        this.itemCard = itemCard;
    }

    private CardView itemCard;


    public CartItem(Meal meal, Location location, int quantity) {
        this.meal = meal;
        this.location = location;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartItem cartItem = (CartItem) o;
        return meal.getMealName().equals(cartItem.getMeal().getMealName()) &&
                location.getName().equals(cartItem.getLocation().getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(meal, location, quantity);
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void addOne(){
        ++quantity;
    }

    public void subtractOne(){
        //don't let user subtract 1 if quantity is already at 1
        quantity = (quantity > 1) ? quantity - 1 : 1;
    }
}
