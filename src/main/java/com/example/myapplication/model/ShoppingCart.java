package com.example.myapplication.model;

import java.util.ArrayList;

public class ShoppingCart {
    private static ShoppingCart cart = null;

    public ArrayList<CartItem> getItems() {
        return items;
    }

    private ArrayList<CartItem> items;

    private ShoppingCart(){
        items = new ArrayList<>();
    };

    public void addItem(CartItem item){
        if(items.contains(item)){
            items.get(items.indexOf(item)).addOne();
        }
        else {
            items.add(item);
        }
    }

    public void deleteItem(CartItem item){
        items.remove(item);
    }

    public void clearCart(){
        items.clear();
    }

    public static ShoppingCart getInstance(){
        if(cart==null){
            cart = new ShoppingCart();
        }
        return cart;
    }


}
