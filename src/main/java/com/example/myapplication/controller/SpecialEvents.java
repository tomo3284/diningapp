package com.example.myapplication.controller;


import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.example.myapplication.R;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SpecialEvents extends Fragment {

    private List<String> imageURLs = new ArrayList();

    public SpecialEvents() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_special_events, container, false);

        if (imageURLs == null || imageURLs.size() < 1) {
            new WebScrapeImages().execute("https://wsu2020tree.s3.amazonaws.com/current-special-event.json", view);
        }

        return view;
    }

    public void listUpImages(View view) {
        ArrayAdapter<String> adapter = new ImageArrayAdapter(getActivity(), 0, imageURLs);
        ListView listView = view.findViewById(R.id.imageListView);
        listView.setAdapter(adapter);
    }

    public void onCreate(Bundle savedInstances) {
        super.onCreate(savedInstances);
    }

    private class WebScrapeImages extends AsyncTask<Object, Void, View> {
        @Override
        protected View doInBackground(Object... args) {
            String urlString = (String) args[0];
            View view = (View) args[1];

            ObjectMapper mapper = new ObjectMapper();
            try {
                URL url = new URL(urlString);
                imageURLs = mapper.readValue(url, new TypeReference<List<String>>() {
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

            return view;
        }

        @Override
        protected void onPostExecute(View view) {
            listUpImages(view);
        }
    }

}
