package com.example.myapplication.controller;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myapplication.model.Location;
import com.example.myapplication.model.MealMenu;
import com.example.myapplication.model.Utilities;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Map;


public class CategoryTabPager extends FragmentStatePagerAdapter {

    private TabLayout tabs;
    private Location location;

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public CategoryTabPager(FragmentManager fm) {
        super(fm);
    }

    public void setTabs(TabLayout tabs) {
        this.tabs = tabs;
    }

    @Override
    public Fragment getItem(int position) {
        CategoryHeading fragment = new CategoryHeading();
        fragment.setLocation(location);
        fragment.setTabs(tabs);
        fragment.setPager(this);
        fragment.setPos(position);
        return fragment;
    }

    @Override
    public int getCount() {
        if (Utilities.getMealsCurrentDateAndTime() == null) {
            return 0;
        }
        return (Utilities.getMealsCurrentDateAndTime().size());
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (Utilities.getMealsCurrentDateAndTime() != null) {
            ArrayList<String> mealTimesOrdered = new ArrayList<>();
            mealTimesOrdered.add("Breakfast");
            mealTimesOrdered.add("Lunch");
            mealTimesOrdered.add("Dinner");
            Map<String, MealMenu> categories = Utilities.getMealsCurrentDateAndTime();
            int i = 0;
            for (Map.Entry<String, MealMenu> entry : categories.entrySet()) {
                String k = entry.getKey();
                if (i == position) {
                    if(mealTimesOrdered.contains(k)){
                        return mealTimesOrdered.get(i);
                    }
                    return k;
                }
                ++i;
            }
        }
        return null;
    }
}
