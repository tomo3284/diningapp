package com.example.myapplication.controller;


import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;

import com.example.myapplication.R;
import com.example.myapplication.model.CartItem;
import com.example.myapplication.model.Location;
import com.example.myapplication.model.Meal;
import com.example.myapplication.model.MealMenu;
import com.example.myapplication.model.ShoppingCart;
import com.example.myapplication.model.Utilities;
import com.google.android.material.tabs.TabLayout;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryHeading extends Fragment {

    private CategoryTabPager pager;
    private int pos;
    private TabLayout tabs;
    private Location location;
    private ShoppingCart cart = ShoppingCart.getInstance();

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public CategoryHeading() {
        // Required empty public constructor
    }

    public void setPager(CategoryTabPager pager) {
        this.pager = pager;
    }

    public void setTabs(TabLayout tabs) {
        this.tabs = tabs;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myView = inflater.inflate(R.layout.fragment_category_heading, container, false);
        NestedScrollView scroller = myView.findViewById(R.id.scroller);
        LinearLayout stuff = myView.findViewById(R.id.layout);
        stuff.setBackgroundColor(ContextCompat.getColor(myView.getContext(), R.color.colorPrimary));
        stuff.setElevation(40);
        scroller.setFillViewport(true);
        Map<String, MealMenu> categories = Utilities.getMealsByDate();
        String categoryName = pager.getPageTitle(pos).toString();
        MealMenu meals = categories.get(categoryName);

        if (meals == null) {
            System.out.println("no meals");
            TextView heading = new TextView(myView.getContext());
            float heightHeading = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 120, getResources().getDisplayMetrics());
            heading.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, (int) heightHeading));
            heading.setTextColor(Color.WHITE);
            heading.setGravity(Gravity.CENTER);
            heading.setTextSize(32);
            heading.setElevation(20);
            heading.setText(R.string.nomeals);
            heading.setTypeface(ResourcesCompat.getFont(myView.getContext(), R.font.raleway_light));
            stuff.addView(heading);
            float scrollPadding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 220, getResources().getDisplayMetrics());
            scroller.setPadding(0, 0, 0, (int) scrollPadding);
            scroller.setNestedScrollingEnabled(false);
        } else {
            LinkedHashMap<String, List<Meal>> sorted = meals.getCategories().entrySet()
                    .stream()
                    .sorted(Map.Entry.comparingByKey())
                    .collect(Collectors.toMap(Map.Entry::getKey,
                            Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
            sorted.forEach((k, v) -> {
                // sort List of Meal
                Collections.sort(v, (a, b) -> a.getMealName().compareTo(b.getMealName()));
                TextView heading = new TextView(myView.getContext());
                float heightHeading = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 65, getResources().getDisplayMetrics());
                FrameLayout.LayoutParams layout = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, (int) heightHeading);
                heading.setLayoutParams(layout);
                heading.setTextColor(Color.WHITE);
                heading.setGravity(Gravity.CENTER);
                heading.setAutoSizeTextTypeWithDefaults(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
                heading.setPadding(50,0,50,0);
                heading.setElevation(20);
                heading.setBackgroundColor(myView.getResources().getColor(R.color.colorPrimaryDark, null));
                k = k.toLowerCase();
                String formatted = "";
                formatted += Character.toUpperCase(k.charAt(0)) + k.substring(1);
                heading.setText(formatted);
                heading.setTypeface(ResourcesCompat.getFont(myView.getContext(), R.font.raleway_semibold));
                stuff.addView(heading);
                int numMealsAdded = 0;
                for (Meal meal : v) {
                    if (CurrentMeals.validMealPerAllergens(meal)) {
                        ++numMealsAdded;
                        LinearLayout mealAndCartLayout = new LinearLayout(myView.getContext());
                        mealAndCartLayout.setOrientation(LinearLayout.HORIZONTAL);
                        mealAndCartLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) heightHeading));
                        LinearLayout mealLayout = new LinearLayout(myView.getContext());
                        mealLayout.setLayoutParams(new LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.MATCH_PARENT,10.0f));
                        TextView mealName = new TextView(myView.getContext());
                        LinearLayout.LayoutParams layoutMeal = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
                        layoutMeal.weight = 2.0f;
                        mealName.setLayoutParams(layoutMeal);
                        mealName.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
                        mealName.setTextColor(Color.WHITE);
                        mealName.setTextSize(24);
                        mealName.setTypeface(ResourcesCompat.getFont(myView.getContext(), R.font.raleway_light));
                        TextView allergenDesc = new TextView(myView.getContext());
                        LinearLayout.LayoutParams layoutAllergenDesc = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
                        layoutAllergenDesc.weight = 2.0f;
                        allergenDesc.setLayoutParams(layoutAllergenDesc);
                        allergenDesc.setGravity(Gravity.CENTER);
                        allergenDesc.setTextColor(Color.WHITE);
                        allergenDesc.setTextSize(20);
                        allergenDesc.setVisibility(View.GONE);
                        allergenDesc.setTypeface(ResourcesCompat.getFont(myView.getContext(), R.font.raleway_light));
                        String mealFormatted = "";
                        mealFormatted += Character.toUpperCase(meal.getMealName().charAt(0)) + meal.getMealName().toLowerCase().substring(1);
                        mealName.setText(String.format("•%s", mealFormatted));
                        float mealLeftPadding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
                        mealName.setPadding((int) mealLeftPadding, 0, 0, 0);
                        List<String> allergies = meal.getAllergies();
                        mealLayout.addView(mealName);
                        mealLayout.addView(allergenDesc);
                        allergies.forEach(allergy -> {
                            ImageView allergyImg = new ImageView(myView.getContext());
                            int drawableID = CurrentMeals.allergyIcons.get(allergy);
                            Drawable d = ResourcesCompat.getDrawable(getResources(), drawableID, null);
                            LinearLayout.LayoutParams allergyLayoutParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
                            allergyLayoutParams.weight = 0.4f;
                            allergyImg.setLayoutParams(allergyLayoutParams);
                            allergyImg.setImageDrawable(d);
                            allergyImg.setOnClickListener(listener -> {
                                int imgId = myView.getResources().getIdentifier(allergyImg.getDrawable().toString(), "drawable", getActivity().getPackageName());
                                allergenDesc.setText(CurrentMeals.allergenDescs.get(drawableID));
                                Thread allergyThread = new Thread() {
                                    @Override
                                    public void run() {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                for (int i = 0; i < mealLayout.getChildCount(); ++i) {
                                                    View view = mealLayout.getChildAt(i);
                                                    if (view instanceof ImageView) {
                                                        view.setVisibility(View.GONE);
                                                    }
                                                }
                                                allergenDesc.setVisibility(View.VISIBLE);
                                            }
                                        });
                                        try {
                                            Thread.sleep(2000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        if (getActivity() == null)
                                            return;
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                allergenDesc.setVisibility(View.GONE);
                                                for (int i = 0; i < mealLayout.getChildCount(); ++i) {
                                                    View view = mealLayout.getChildAt(i);
                                                    if (view instanceof ImageView) {
                                                        view.setVisibility(View.VISIBLE);
                                                    }
                                                }
                                            }
                                        });
                                    }
                                };
                                allergyThread.start();
                            });

                            mealLayout.addView(allergyImg);
                        });
                        mealAndCartLayout.addView(mealLayout);
                        if (location.getName().equals("TJ Bistro")) {
                            ImageView addTo = new ImageView(myView.getContext());
                            LinearLayout.LayoutParams addLayoutParam = new LinearLayout.LayoutParams(
                                0, LinearLayout.LayoutParams.MATCH_PARENT);
                            addLayoutParam.weight = 2.0f;
                            addTo.setLayoutParams(addLayoutParam);
                            Drawable d = ResourcesCompat.getDrawable(getResources(), R.drawable.addicon2, null);
                            addTo.setImageDrawable(d);
                            addTo.setPadding(15,0,15,0);
                            addTo.setVisibility(View.VISIBLE);
                            addTo.setClickable(true);

                            mealAndCartLayout.addView(addTo);

                            addTo.setOnClickListener(listener -> {
                                cart.addItem(new CartItem(meal, location, 1));
                                TextView customToast = (TextView)inflater.inflate(R.layout.custom_toast,container,false);
                                Toast toast = Toast.makeText(myView.getContext(),"Meal added to cart.", Toast.LENGTH_SHORT);
                                toast.setView(customToast);
                                toast.show();
                            });

                        }
                        stuff.addView(mealAndCartLayout);
                    }
                }
                if (numMealsAdded == 0) {
                    //remove the heading because the user filtered out the meals
                    stuff.removeView(heading);
                }
            });
            scroller.setNestedScrollingEnabled(false);
            scroller.setFillViewport(true);
        }
        return myView;
    }
}
