package com.example.myapplication.controller;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplication.model.Meal;

import java.util.List;

public class OrderObserver extends ViewModel {
    private final MutableLiveData<List<Meal>> order = new MutableLiveData<>();

    public void setOrder(List<Meal> order) {
        this.order.setValue(order);
    }

    public LiveData<List<Meal>> getOrder() {
        return order;
    }
}