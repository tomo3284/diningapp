package com.example.myapplication.controller;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText et_email, et_password;
    private Button login_btn;
    private TextView logout_tv;
    private ProgressBar progressBar;

    private String studentEmail;
    private String studentPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get the UI element by defined ID and store that to variable
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        progressBar = findViewById(R.id.progressBar_signActivity);

        // event listener
        login_btn = findViewById(R.id.login_btn_signActivity);
        logout_tv = findViewById(R.id.logout);
        login_btn.setOnClickListener(this);
        logout_tv.setOnClickListener(this);

        // If this Activity is opened from another activity,
        // leave intent code below here.
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.logout:
                // I don't know if we need this...
                break;

            case R.id.login_btn_signActivity:
                studentEmail = et_email.getText().toString();
                studentPassword = et_password.getText().toString();
                if (isValidEmailAndPassword()) {
                    Toast.makeText(LoginActivity.this, "Successfully logged in", Toast.LENGTH_LONG).show();
                    // properly login if input email and password is valid
                } else {
                    Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_LONG).show();
                    // do something if input email or password is not valid
                }
                break;
        }
    }

    private boolean isValidEmailAndPassword() {
        // check validity of input email and password from database
        return false;
    }

}
