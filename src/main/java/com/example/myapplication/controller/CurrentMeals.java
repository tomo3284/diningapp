package com.example.myapplication.controller;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.cardview.widget.CardView;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.myapplication.R;
import com.example.myapplication.model.Location;
import com.example.myapplication.model.Meal;
import com.example.myapplication.model.Utilities;
import com.google.android.material.tabs.TabLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import static android.widget.LinearLayout.VERTICAL;

/**
 * A simple {@link Fragment} subclass.
 */
public class CurrentMeals extends Fragment {

    public static HashMap<Integer, String> allergenDescs = new HashMap<>();
    public static HashMap<String, Integer> allergyIcons = new HashMap<>();
    static Drawable grey;
    static Drawable green;
    private static HashMap<String, String> cardNameLoc = new HashMap<>();
    private static HashMap<TextView, ArrayList<TextView>> buttonGroups = new HashMap<>();
    private static NestedScrollView filterLayout;
    private HashMap<ArrayList<TextView>, Boolean> multiselectable = new HashMap<>();
    private ArrayList<String> currentSearchResults;
    private boolean mealSortAsc = true;
    private boolean locSortAsc = true;
    private int daysSinceCurrent = 0;

    public CurrentMeals() {
        // Required empty public constructor
    }

    public static boolean validMealPerAllergens(Meal meal) {
        TextView nuts = filterLayout.findViewById(R.id.filterANuts);
        ArrayList<TextView> allergyGroup = buttonGroups.get(nuts);
        boolean validMeal = false;
        int numGreen = 0;
        for (TextView allergyView : allergyGroup) {
            String allergy = allergyView.getText().toString();
            //need to change casing to match allergen list in meal
            switch (allergy) {
                case "Whole grain":
                    allergy = "WholeGrain";
                    break;
                case "Local produce":
                    allergy = "LocalProduce";
                    break;
                case "Has nuts":
                    allergy = "ContainsNuts";
                    break;
                default:
                    break;
            }
            if (allergyView.getBackground().getConstantState().equals(green.getConstantState())) {
                ++numGreen;
                if (meal.getAllergies().contains(allergy)) {
                    validMeal = true;
                }
            }
        }
        //if no allergen tags are selected, meal should always be valid
        return (numGreen == 0) || validMeal;
    }

    private Map<Meal, ArrayList<Location>> sortMeals(View myView) {
        //sort by meal name then location name
        Utilities.curDate = getDateFromCurrentSelection();
        Utilities.setMeals();
        HashMap<Meal, ArrayList<Location>> meals = Utilities.meals;
        Map<Meal, ArrayList<Location>> sortedMap = new HashMap<>();
        for (Entry<Meal, ArrayList<Location>> entry : meals.entrySet()) {
            Meal meal = entry.getKey();
            ArrayList<Location> locations = entry.getValue();
            locations.sort((loc1, loc2) -> {
                String s1 = loc1.getName();
                String s2 = loc2.getName();
                if (locSortAsc) {
                    return s1.compareTo(s2);
                }
                return s2.compareTo(s1);
            });
            TextView ely = myView.findViewById(R.id.filterByLocEly);
            ArrayList<TextView> locButtons = buttonGroups.get(ely);
            ArrayList<String> include = new ArrayList<>();
            locButtons.forEach(view -> {
                if (view.getBackground().getConstantState().equals(green.getConstantState())) {
                    include.add(view.getText().toString());
                }
            });
            ArrayList<Location> filteredLocs = new ArrayList<>();
            locations.forEach(location -> {
                if (include.contains(location.getName())) {
                    filteredLocs.add(location);
                }
            });
            if (validMealPerAllergens(meal)) {
                sortedMap.put(meal, filteredLocs);
            }
        }
        Comparator<Entry<Meal, ArrayList<Location>>> valueComparator = (e1, e2) -> {
            String s1 = e1.getKey().getMealName();
            String s2 = e2.getKey().getMealName();
            if (mealSortAsc) {
                return s1.compareTo(s2);
            }
            return s2.compareTo(s1);
        };
        sortedMap =
                sortedMap.entrySet().stream().
                        sorted(valueComparator).
                        collect(Collectors.toMap(Entry::getKey, Entry::getValue,
                                (e1, e2) -> e1, LinkedHashMap::new));


        return sortedMap;
    }

    private void turnOn(View myView, TextView button) {
        button.setBackgroundResource(R.drawable.rounded_green);
    }

    private void turnOff(View myView, TextView button) {
        button.setBackgroundResource(R.drawable.rounded_white);
    }

    private View.OnClickListener filterButton(View myView, TextView button) {
        return listener -> {
            String name = myView.getResources().getResourceEntryName(button.getId());
            ArrayList<TextView> group = buttonGroups.get(button);
            boolean canMultiSelect = multiselectable.get(group);
            if (!canMultiSelect) {
                //turn every other button grey in the button's group and make current one green
                group.forEach(buttonInGroup -> {
                    turnOff(myView, buttonInGroup);
                });
                turnOn(myView, button);
                //truncating the last digit on the name of a day because I'm lazy
                name = (name.contains("day")) ? name.substring(0, name.length() - 1) : name;
                System.out.println(name);
                switch (name) {
                    case "day":
                        daysSinceCurrent = buttonGroups.get(button).indexOf(button);
                        break;
                    case "filterMealAZ":
                        mealSortAsc = true;
                        break;
                    case "filterMealZA":
                        mealSortAsc = false;
                        break;
                    case "filterLocAZ":
                        locSortAsc = true;
                        break;
                    case "filterLocZA":
                        locSortAsc = false;
                        break;
                    default:
                        break;
                }
            } else {
                //turn it on or off, doesn't matter
                Drawable background = button.getBackground();
                if (background.getConstantState().equals(grey.getConstantState())) {
                    button.setBackgroundResource(R.drawable.rounded_green);
                } else if (background.getConstantState().equals(green.getConstantState())) {
                    button.setBackgroundResource(R.drawable.rounded_white);
                }
            }
        };
    }

    private View.OnClickListener getListener(View myView, CardView location) {
        return view -> {
            LinearLayout locLayout = myView.findViewById(R.id.locLayout);
            NestedScrollView mainScroll = myView.findViewById(R.id.mainScroll);
            int indexBtn = locLayout.indexOfChild(location);
            if (locLayout.getChildAt(indexBtn + 1) instanceof LinearLayout) {
                locLayout.removeViewAt(indexBtn + 1);
                CustomScrollView customScroller = (CustomScrollView) mainScroll;
                customScroller.setEnableScrolling(true);
            } else {
                String resName = location.getResources().getResourceEntryName(location.getId());
                String locName = cardNameLoc.get(resName);
                Utilities.setCurrentLocation(Utilities.getLocation(locName.trim()));
                TabLayout categoryTab = new TabLayout(myView.getContext());
                categoryTab = (TabLayout) LayoutInflater.from(myView.getContext()).inflate(R.layout.custom_tab, categoryTab, false);
                ViewPager pager = new ViewPager(myView.getContext());
                pager = (ViewPager) LayoutInflater.from(myView.getContext()).inflate(R.layout.custom_pager, pager, false);
                pager.getLayoutParams().height = (int) (location.getLayoutParams().height * 3.5);
                android.view.Display display = ((android.view.WindowManager) myView.getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                float offsetY = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 195, getResources().getDisplayMetrics());
                pager.getLayoutParams().height = size.y - location.getLayoutParams().height - (int) offsetY;
                CategoryTabPager tabPager = new CategoryTabPager(getChildFragmentManager());
                tabPager.setLocation(Utilities.currentLocation);
                LinearLayout tabPagerLayout = new LinearLayout(myView.getContext());
                tabPagerLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tabPagerLayout.setOrientation(VERTICAL);
                categoryTab.setupWithViewPager(pager);
                pager.setAdapter(tabPager);
                if (categoryTab.getTabCount() == 0) {
                    TextView heading = new TextView(myView.getContext());
                    heading.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
                    heading.setTextColor(Color.WHITE);
                    heading.setGravity(Gravity.CENTER_HORIZONTAL);
                    heading.setTextSize(32);
                    heading.setText(R.string.nomeals);
                    heading.setTypeface(ResourcesCompat.getFont(myView.getContext(), R.font.raleway_semibold));
                    float offsetYHeading = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 145, getResources().getDisplayMetrics());
                    heading.getLayoutParams().height = size.y - location.getLayoutParams().height - (int) offsetYHeading;
                    tabPagerLayout.setBackgroundColor(Color.parseColor("#007DCF"));
                    tabPagerLayout.addView(heading);
                } else {
                    tabPagerLayout.addView(categoryTab);
                    tabPagerLayout.addView(pager);
                    tabPager.setTabs(categoryTab);
                }
                locLayout.addView(tabPagerLayout, locLayout.indexOfChild(location) + 1);
                mainScroll.post(() -> mainScroll.scrollTo(0, location.getTop()));
                CustomScrollView scroll = (CustomScrollView) mainScroll;
                scroll.setEnableScrolling(false);
                mainScroll.setNestedScrollingEnabled(false);
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myView = inflater.inflate(R.layout.fragment_current_meals, container, false);

        allergyIcons.put("Vegan", R.drawable.veganicon);
        allergyIcons.put("Vegetarian", R.drawable.vegetarianicon);
        allergyIcons.put("LocalProduce", R.drawable.localproduceicon);
        allergyIcons.put("WholeGrain", R.drawable.wholegrainicon);
        allergyIcons.put("ContainsNuts", R.drawable.peanuticon);
        allergyIcons.put("Sustainable", R.drawable.sustainableicon);

        allergenDescs.put(R.drawable.veganicon, "Vegan meal");
        allergenDescs.put(R.drawable.vegetarianicon, "Vegetarian meal");
        allergenDescs.put(R.drawable.localproduceicon, "Local produce meal");
        allergenDescs.put(R.drawable.wholegrainicon, "Meal is whole grain");
        allergenDescs.put(R.drawable.peanuticon, "Meal contains nuts");
        allergenDescs.put(R.drawable.sustainableicon, "Meal is sustainable");

        green = AppCompatResources.getDrawable(myView.getContext(), R.drawable.rounded_green);
        grey = AppCompatResources.getDrawable(myView.getContext(), R.drawable.rounded_white);

        SearchView searchBar = myView.findViewById(R.id.searchBar);
        TextView exit = myView.findViewById(R.id.searchResExit);

        TextView elyCardText = myView.findViewById(R.id.elyName);
        TextView timCardText = myView.findViewById(R.id.timJeanneName);
        TextView gardenCardText = myView.findViewById(R.id.gardenName);
        TextView marketCardText = myView.findViewById(R.id.marketName);
        TextView wilsonCardText = myView.findViewById(R.id.wilsonName);
        TextView TJCardText = myView.findViewById(R.id.tjName);

        CardView elyCard = myView.findViewById(R.id.elyCard);
        CardView timCard = myView.findViewById(R.id.timJeanneCard);
        CardView gardenCard = myView.findViewById(R.id.gardenCard);
        CardView marketCard = myView.findViewById(R.id.marketCard);
        CardView wilsonCard = myView.findViewById(R.id.wilsonCard);
        CardView TJCard = myView.findViewById(R.id.tjCard);

        HashMap<CardView, TextView> locationCardsText = new HashMap<>();
        locationCardsText.put(elyCard, elyCardText);
        locationCardsText.put(gardenCard, gardenCardText);
        locationCardsText.put(marketCard, marketCardText);
        locationCardsText.put(TJCard, TJCardText);
        locationCardsText.put(wilsonCard, wilsonCardText);
        locationCardsText.put(timCard, timCardText);
        locationCardsText.forEach((card,loc)->{
            RelativeLayout layout = (RelativeLayout)card.getChildAt(0);
            TextView openText = (TextView)layout.getChildAt(1);
            if(Utilities.isOpen(Utilities.getLocation(loc.getText().toString()),Utilities.getCurrentTimeStandard())){
                card.setAlpha(1.0f);
                openText.setText("Open now");
            }
            else {
                card.setAlpha(0.75f);
                openText.setText("Closed now");

            }
        });

        filterLayout = (NestedScrollView) inflater.inflate(R.layout.filter_layout, container, false);
        setupFilterGroups(filterLayout);
        LinearLayout locLayout = myView.findViewById(R.id.locLayout);
        CustomScrollView mainScroll = myView.findViewById(R.id.mainScroll);
        TextView apply = filterLayout.findViewById(R.id.apply);
        CardView filterBtn = myView.findViewById(R.id.filterCard);
        Point size = new Point();
        android.view.Display display = ((android.view.WindowManager) myView.getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        display.getSize(size);
        float offsetY = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 145, getResources().getDisplayMetrics());
        LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) (size.y - offsetY));
        params.setMargins(0, 0, 0, (int) (offsetY / 20));
        filterLayout.setLayoutParams(params);
        locLayout.addView(filterLayout, 0);
        apply.setOnClickListener(listener -> {
            mainScroll.setEnableScrolling(true);
            CharSequence query = searchBar.getQuery();
            searchBar.setQuery("", false);
            searchBar.setQuery(query, false);
            filterLayout.setVisibility(View.GONE);
            filterBtn.setVisibility(View.VISIBLE);
            searchBar.setVisibility(View.VISIBLE);
            //make green locations visible and grey ones gone
            TextView ely = filterLayout.findViewById(R.id.filterByLocEly);
            ArrayList<TextView> locGroup = buttonGroups.get(ely);
            boolean makeVisible;
            for (TextView location : locGroup) {
                if (location.getBackground().getConstantState().equals(grey.getConstantState())) {
                    makeVisible = false;
                } else {
                    makeVisible = true;
                }
                for (int i = 0; i < locLayout.getChildCount(); ++i) {
                    View view = locLayout.getChildAt(i);
                    if (view instanceof CardView) {
                        CardView card = (CardView) view;
                        TextView cardText = locationCardsText.get(card);
                        if (cardText.getText().toString().equals(location.getText().toString())) {
                            card.setVisibility((makeVisible) ? View.VISIBLE : View.GONE);
                        }
                    }
                }
            }
        });
        filterLayout.setVisibility(View.GONE);
        filterBtn.setOnClickListener(listener -> {
            filterBtn.setVisibility(View.GONE);
            searchBar.setVisibility(View.GONE);
            filterLayout.setVisibility(View.VISIBLE);
            mainScroll.setEnableScrolling(false);
            filterLayout.setNestedScrollingEnabled(false);
        });
        cardNameLoc.put("elyCard", "Ely Harvest");
        cardNameLoc.put("timJeanneCard", "Tim & Jeanne's Dining Commons");
        cardNameLoc.put("gardenCard", "Garden Café ");
        cardNameLoc.put("marketCard", "Marketplace");
        cardNameLoc.put("tjCard", "TJ Bistro");
        cardNameLoc.put("wilsonCard", "Wilson Café ");
        for (int i = 0; i < locLayout.getChildCount(); ++i) {
            View view = locLayout.getChildAt(i);
            if (view instanceof CardView) {
                CardView card = (CardView) view;
                view.setOnClickListener(getListener(myView, card));
            }
        }
        NestedScrollView resultsScroll = myView.findViewById(R.id.searchLayoutScroll);
        LinearLayout results = myView.findViewById(R.id.searchLayout);
        searchBar.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                resultsScroll.setVisibility(View.GONE);
                results.removeAllViews();
                return false;
            }
        });
        searchBar.setOnCloseListener(new SearchView.OnCloseListener() {

            @Override
            public boolean onClose() {
                mainScroll.setEnableScrolling(true);
                results.removeAllViews();
                resultsScroll.setVisibility(View.GONE);
                exit.setVisibility(View.GONE);
                return false;
            }
        });
        searchBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String meal) {
                CharSequence query = searchBar.getQuery();
                searchBar.setQuery("", false);
                searchBar.setQuery(query, false);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String text) {
                results.removeAllViews();
                resultsScroll.setVisibility(View.GONE);
                exit.setVisibility(View.GONE);
                boolean first = true;
                if (text.length() > 2) //no point in querying one character
                {
                    float ten = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
                    Map<Meal, ArrayList<Location>> stuff = sortMeals(myView);
                    resultsScroll.setVisibility(View.VISIBLE);
                    exit.setVisibility(View.VISIBLE);
                    resultsScroll.getLayoutParams().height = (int) (size.y - ten * 28);
                    exit.setOnClickListener(listener -> {
                        results.removeAllViews();
                        resultsScroll.setVisibility(View.GONE);
                        exit.setVisibility(View.GONE);
                        mainScroll.setEnableScrolling(true);
                    });
                    mainScroll.setEnableScrolling(false);
                    for (Map.Entry<Meal, ArrayList<Location>> entry : stuff.entrySet()) {
                        Meal meal = entry.getKey();
                        ArrayList<Location> locations = entry.getValue();
                        if (meal.getMealName().toLowerCase().contains(text)) {
                            for (Location location : locations) {
                                CardView result = (CardView) inflater.inflate(R.layout.search_result, container, false);
                                TextView allergenDesc = result.findViewById(R.id.resAllergyDefault);
                                allergenDesc.setVisibility(View.GONE);
                                LinearLayout resultsAllergies = result.findViewById(R.id.searchResAllergies);
                                List<String> allergies = meal.getAllergies();
                                int numAllergies = 0;
                                for (String allergy : allergies) {
                                    ++numAllergies;
                                    ImageView allergyImg = new ImageView(myView.getContext());
                                    int drawableID = allergyIcons.get(allergy);
                                    Drawable d = ResourcesCompat.getDrawable(getResources(), drawableID, null);
                                    LayoutParams allergyLayoutParams = new LayoutParams(0, LayoutParams.MATCH_PARENT);
                                    allergyLayoutParams.weight = 0.4f;
                                    allergyImg.setLayoutParams(allergyLayoutParams);
                                    allergyImg.setImageDrawable(d);
                                    allergyImg.setOnClickListener(listener -> {
                                        int imgId = myView.getResources().getIdentifier(allergyImg.getDrawable().toString(), "drawable", getActivity().getPackageName());
                                        Thread allergyThread = new Thread() {
                                            @Override
                                            public void run() {
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        for (int i = 0; i < resultsAllergies.getChildCount(); ++i) {
                                                            View view = resultsAllergies.getChildAt(i);
                                                            if (view instanceof ImageView) {
                                                                view.setVisibility(View.GONE);
                                                            }
                                                        }
                                                        allergenDesc.setVisibility(View.VISIBLE);
                                                        allergenDesc.setText(allergenDescs.get(drawableID));
                                                    }
                                                });
                                                try {
                                                    Thread.sleep(2000);
                                                } catch (InterruptedException e) {
                                                    e.printStackTrace();
                                                }
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        allergenDesc.setVisibility(View.GONE);
                                                        for (int i = 0; i < resultsAllergies.getChildCount(); ++i) {
                                                            View view = resultsAllergies.getChildAt(i);
                                                            if (view instanceof ImageView) {
                                                                view.setVisibility(View.VISIBLE);
                                                            }
                                                        }
                                                    }
                                                });
                                            }
                                        };
                                        allergyThread.start();
                                    });
                                    resultsAllergies.addView(allergyImg);
                                }
                                allergenDesc.setVisibility(numAllergies == 0 ? View.VISIBLE : View.GONE);
                                TextView mealName = result.findViewById(R.id.searchResMealName);
                                LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, (int) (ten * 21));
                                float topMargin = first ? ten / 2 : 0;
                                first = false;
                                layoutParams.setMargins((int) (ten / 2), (int) topMargin, (int) (ten / 2), (int) (ten / 2));
                                result.setLayoutParams(layoutParams);
                                resultsScroll.setNestedScrollingEnabled(false);
                                //results.setBackground(AppCompatResources.getDrawable(myView.getContext(), R.drawable.rounded));
                                //results.setBackground(null);
                                mealName.setText(meal.getMealName());
                                TextView locName = result.findViewById(R.id.searchResLocName);
                                locName.setText(location.getName());
                                results.addView(result);
                            }
                        }
                    }
                }
                return false;
            }
        });
        return myView;
    }

    private void setupFilterGroups(View myView) {

        TextView filterMealAZ = myView.findViewById(R.id.filterMealAZ);
        TextView filterMealZA = myView.findViewById(R.id.filterMealZA);

        TextView filterLocAZ = myView.findViewById(R.id.filterLocAZ);
        TextView filterLocZA = myView.findViewById(R.id.filterLocZA);

        TextView filterAVegan = myView.findViewById(R.id.filterAVegan);
        TextView filterAVegetarian = myView.findViewById(R.id.filterAVege);
        TextView filterASustainable = myView.findViewById(R.id.filterASus);
        TextView filterAWholeGrain = myView.findViewById(R.id.filterAWholeGrain);
        TextView filterANuts = myView.findViewById(R.id.filterANuts);
        TextView filterALocalProduce = myView.findViewById(R.id.filterALocProd);

        TextView filterByLocEly = myView.findViewById(R.id.filterByLocEly);
        TextView filterByLocMarket = myView.findViewById(R.id.filterByLocMarket);
        TextView filterByLocTim = myView.findViewById(R.id.filterByLocTim);
        TextView filterByLocGarden = myView.findViewById(R.id.filterByLocGard);
        TextView filterByLocWilson = myView.findViewById(R.id.filterByLocWils);
        TextView filterByLocTJ = myView.findViewById(R.id.filterByLocTJ);

        TextView day1 = myView.findViewById(R.id.day1);
        TextView day2 = myView.findViewById(R.id.day2);
        TextView day3 = myView.findViewById(R.id.day3);
        TextView day4 = myView.findViewById(R.id.day4);
        TextView day5 = myView.findViewById(R.id.day5);
        TextView day6 = myView.findViewById(R.id.day6);
        TextView day7 = myView.findViewById(R.id.day7);

        ArrayList<TextView> sortMealGroup = new ArrayList<>(Arrays.asList(filterMealAZ, filterMealZA));
        ArrayList<TextView> sortLocGroup = new ArrayList<>(Arrays.asList(filterLocAZ, filterLocZA));
        ArrayList<TextView> filterAllergyGroup = new ArrayList<>(Arrays.asList(
                filterANuts, filterALocalProduce, filterASustainable, filterAVegan, filterAVegetarian, filterAWholeGrain
        ));
        ArrayList<TextView> filterByLocGroup = new ArrayList<>(Arrays.asList(
                filterByLocEly, filterByLocGarden, filterByLocMarket, filterByLocTim, filterByLocTJ, filterByLocWilson
        ));
        ArrayList<TextView> dayGroup = new ArrayList<>(Arrays.asList(
                day1, day2, day3, day4, day5, day6, day7
        ));

        buttonGroups.put(filterMealAZ, sortMealGroup);
        buttonGroups.put(filterMealZA, sortMealGroup);

        buttonGroups.put(filterLocAZ, sortLocGroup);
        buttonGroups.put(filterLocZA, sortLocGroup);

        buttonGroups.put(filterANuts, filterAllergyGroup);
        buttonGroups.put(filterAVegan, filterAllergyGroup);
        buttonGroups.put(filterAVegetarian, filterAllergyGroup);
        buttonGroups.put(filterAWholeGrain, filterAllergyGroup);
        buttonGroups.put(filterASustainable, filterAllergyGroup);
        buttonGroups.put(filterALocalProduce, filterAllergyGroup);

        buttonGroups.put(filterByLocEly, filterByLocGroup);
        buttonGroups.put(filterByLocGarden, filterByLocGroup);
        buttonGroups.put(filterByLocTJ, filterByLocGroup);
        buttonGroups.put(filterByLocTim, filterByLocGroup);
        buttonGroups.put(filterByLocWilson, filterByLocGroup);
        buttonGroups.put(filterByLocMarket, filterByLocGroup);

        buttonGroups.put(day1, dayGroup);
        buttonGroups.put(day2, dayGroup);
        buttonGroups.put(day3, dayGroup);
        buttonGroups.put(day4, dayGroup);
        buttonGroups.put(day5, dayGroup);
        buttonGroups.put(day6, dayGroup);
        buttonGroups.put(day7, dayGroup);

        buttonGroups.forEach((button, list) -> {
            button.setOnClickListener(filterButton(myView, button));
        });

        //whether or not a user can select multiple buttons in the same group as the greenlit state
        multiselectable.put(sortMealGroup, false);
        multiselectable.put(sortLocGroup, false);
        multiselectable.put(filterAllergyGroup, true);
        multiselectable.put(filterByLocGroup, true);
        multiselectable.put(dayGroup, false);
    }

    private String getDateFromCurrentSelection() {
        //indexes of dayGroup are days since the current date, which I will grab here
        Calendar c = Calendar.getInstance();
        c.set(2020, 2, 23);
        c.add(Calendar.DATE, daysSinceCurrent);
        SimpleDateFormat formatter = new SimpleDateFormat("M/d/yyyy", Locale.US);
        return formatter.format(c.getTime());
    }
}
