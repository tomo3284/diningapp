package com.example.myapplication.controller;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


public class MainTabPager extends FragmentPagerAdapter {

    public MainTabPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new SpecialEvents();
            case 1:
                return new CurrentMeals();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Special events";
            case 1:
                return "Current meals";
            default:
                return "error";
        }
    }
}
