package com.example.myapplication.controller;


import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.transition.Slide;
import android.transition.Transition;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.example.myapplication.R;
import com.example.myapplication.model.Utilities;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MainTabPager swipePager;
    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private ImageButton shoppingCart;
    private FragmentManager fm;
    private ConstraintLayout mainConstraint;
    private ActionBarDrawerToggle drawerToggle;
    private boolean isInFront = true;

    // Make sure to be using androidx.appcompat.app.ActionBarDrawerToggle version.
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onResume() {
        super.onResume();
        isInFront = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isInFront = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Transition fade = new Slide(Gravity.END);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        fade.excludeTarget(R.id.toolbar, true);
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        fade.excludeTarget(android.R.id.navigationBarBackground, true);
        fade.setDuration(750);
        getWindow().setExitTransition(fade);
        getWindow().setEnterTransition(fade);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set a Toolbar to replace the ActionBar.
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Swipe bar from before that Brian implemented.
        tabLayout = findViewById(R.id.tabLayout);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 1) {
                    checkHours();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager = findViewById(R.id.viewPager);
        swipePager = new MainTabPager(getSupportFragmentManager());
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(swipePager);
        GetData reader = new GetData();
        reader.execute();
        try {
            reader.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        mDrawer = findViewById(R.id.drawer_layout);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    private void checkHours() {

    }

    //TODO get the class from each fragment to set it as the current main one
    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Fragment fragment = null;
        Class fragmentClass;
        switch (menuItem.getItemId()) {
            case R.id.nav_diningCommons_fragment:
                //  fragmentClass = DiningCommonsFragment.class;
                break;
            case R.id.nav_elyHarvest_fragment:
                //fragmentClass = SecondFragment.class;
                break;
            case R.id.nav_gardenCafe_fragment:
                //fragmentClass = ThirdFragment.class;
                break;
            case R.id.nav_marketplace_fragment:
                //fragmentClass = Fragment.class;
                break;
            case R.id.nav_tjBistro_fragment:
                //fragmentClass =
                break;
            case R.id.nav_wilsonCafe_fragment:
                //fragmentClass=
                break;
            case R.id.nav_feedback_fragment:
                //fragmentClass =
                break;
            default:
                //fragmentClass = FirstFragment.class;
        }

        try {
            //fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        // fragmentManager.beginTransaction().replace(R.id.flContent, new DiningCommonsFragment()).commit();

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        setTitle(menuItem.getTitle());
        // Close the navigation drawer
        //  mDrawer.closeDrawers();
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.homeButton:
            case R.id.TitleHomeButton:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent,
                        ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                break;
            case R.id.menuButton:
                if (mDrawer.isDrawerOpen(GravityCompat.START)) {
                    mDrawer.closeDrawer(GravityCompat.START);
                } else mDrawer.openDrawer(GravityCompat.START);
                break;

            case R.id.navElyCard:
                Bundle bundle = new Bundle();
                bundle.putString("location", "Ely Harvest");
                Fragment elyPageFragment = new LocationPage();
                elyPageFragment.setArguments(bundle);
                setDrawerTransactionCommit(elyPageFragment);
                break;

            case R.id.navGardenCard:
                Bundle bundle1 = new Bundle();
                bundle1.putString("location", "Garden Café");
                Fragment gardenPageFragment = new LocationPage();
                gardenPageFragment.setArguments(bundle1);
                setDrawerTransactionCommit(gardenPageFragment);
                break;

            case R.id.navTimJeanneCard:
                Bundle bundle2 = new Bundle();
                bundle2.putString("location", "Tim & Jeanne's Dining Commons");
                Fragment timJeannePageFragment = new LocationPage();
                timJeannePageFragment.setArguments(bundle2);
                setDrawerTransactionCommit(timJeannePageFragment);
                break;

            case R.id.navWilsonCard:
                Bundle bundle3 = new Bundle();
                bundle3.putString("location", "Wilson Café");
                Fragment wilsonPageFragment = new LocationPage();
                wilsonPageFragment.setArguments(bundle3);
                setDrawerTransactionCommit(wilsonPageFragment);
                break;

            case R.id.navTJCard:
                Bundle bundle4 = new Bundle();
                bundle4.putString("location", "TJ Bistro");
                Fragment tjPageFragment = new LocationPage();
                tjPageFragment.setArguments(bundle4);
                setDrawerTransactionCommit(tjPageFragment);
                break;

            case R.id.navMarketCard:
                Bundle bundle5 = new Bundle();
                bundle5.putString("location", "Marketplace");
                Fragment marketPageFragment = new LocationPage();
                marketPageFragment.setArguments(bundle5);
                setDrawerTransactionCommit(marketPageFragment);
                break;

            case R.id.botConstraint:
                Utilities.emailFeedback(view);
                break;

            case R.id.shoppingCart:
                Bundle shoppingBundle = new Bundle();
                Fragment shoppingCartFragment = new ShoppingCartFragment();
                shoppingCartFragment.setArguments(shoppingBundle);
                setDrawerTransactionCommit(shoppingCartFragment);
        }
    }

    public void setDrawerTransactionCommit(Fragment locationPageFragment) {
        mDrawer.closeDrawer(GravityCompat.START);
        tabLayout.setVisibility(View.GONE);
        mainConstraint = findViewById(R.id.mainConstraint);
        mainConstraint.removeView(findViewById(R.id.viewPager));
        mainConstraint.removeView(findViewById(R.id.tabLayout));
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.mainConstraint, locationPageFragment); // give your fragment container id in first parameter
        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
        transaction.commit();
    }
}
