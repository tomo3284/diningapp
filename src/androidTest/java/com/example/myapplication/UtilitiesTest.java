package com.example.myapplication;

import com.example.myapplication.model.Location;
import com.example.myapplication.model.MealMenu;
import com.example.myapplication.model.Utilities;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertTrue;

public class UtilitiesTest {

    static List<Location> diningMenus = null;
    static final String locnameEly = "Ely Harvest";
    static final String locnameGarden = "Garden Café";
    static final String locnameMarket = "Marketplace";
    static final String locnameTimJeanne = "Tim & Jeanne's Dining Commons";
    static final String locnameTJ = "TJ Bistro";
    static final String locnameWilson = "Wilson Café";

    @BeforeClass
    public static void initialize() {
        diningMenus = null;
        ObjectMapper mapper = new ObjectMapper();

        try {
            URL url = new URL("https://wsu2020tree.s3.amazonaws.com/current-menu.json");
            diningMenus = mapper.readValue(url, new TypeReference<List<Location>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        Utilities.locations = diningMenus;
        Utilities.curDate = "3/23/2020";
    }

    @Before
    public void initBeforeEach() {
        // need to set this, because testing setter for Util would override location
        Utilities.locations = diningMenus;
    }

    @Test
    public void getLocation() {
        Location ely = Utilities.getLocation(locnameEly);
        assertTrue(ely != null);
        assertTrue(ely.getName().equals(locnameEly));

        Location nonExistLocation = Utilities.getLocation(locnameEly + " ");
        assertTrue(nonExistLocation == null);

        Location nonExistLocation2 = Utilities.getLocation("Wilson Cafe");
        assertTrue(nonExistLocation2 == null);

        Location nonExistlocation3 = Utilities.getLocation(" ");
        assertTrue(nonExistlocation3 == null);
    }

    @Test
    public void setCurrentLocation() {
        Location testLoc = new Location();
        testLoc.setName("Test name");
        testLoc.setDescription("Test description");
        testLoc.setUrl("Test URL");
        Utilities.setCurrentLocation(testLoc);

        Location location = Utilities.getLocation("Test name");
        assert (Utilities.currentLocation != null);
        assert (location != null);
        Utilities.setCurrentLocation(null);
        assert (Utilities.currentLocation == null);
    }

    //TODO: More test
    @Test
    public void getMealsByDateAndTime() {
//    Date currentDate = Calendar.getInstance().getTime();
//    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
//    String todayDate = Utilities.getCurrentDate();
//    String testDate = dateFormat.format(currentDate);

        // fake test for hard coded value because fake website date is not updating
        String fakeTestDate = "3/23/2020";
        Location testLocation = Utilities.getLocation(locnameMarket);
        assertTrue(testLocation != null);
        Utilities.setCurrentLocation(testLocation);
        MealMenu mm = Utilities.getMealsByDateAndTime(fakeTestDate, "09:00");
        assertTrue(mm.getCategories() != null);
    }

    //TODO: More test
    @Test
    public void getMealsCurrentDateAndTime() {

    }

    @Test
    public void getCurrentDate() {
        Date currentDate = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        String testDate = Utilities.curDate;
        String fakeTodayDate = "3/23/2020";
        assertTrue(fakeTodayDate.equals(testDate));
    }
}