package com.example.myapplication;

import com.example.myapplication.model.Location;
import com.example.myapplication.model.LocationInfo;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocationTest {

    @Test
    public void getMenuCollection() {
    }

    @Test
    public void setAndGetLocationInfo() {
        LocationInfo locInfo = new LocationInfo();
        Map<String, List<String>> hours = new HashMap<>();
        List<String> testVals = new ArrayList<>();
        testVals.add("Burger");
        testVals.add("Hotdog");
        testVals.add("Chicken");
        hours.put("Lunch", testVals);
        List<String> testVals2 = new ArrayList<>();
        testVals2.add("Pizza");
        testVals2.add("Pasta");
        testVals2.add("Soup");
        hours.put("Dinner", testVals2);
        locInfo.setHours(hours);
        Location loc = new Location();
        loc.setLocationInfo(locInfo);
        Assert.assertTrue(loc.getLocationInfo() != null);
        assert (loc.getLocationInfo().equals(locInfo));
    }

    @Test
    public void equals1() {
    }

    @Test
    public void hashCode1() {
    }
}