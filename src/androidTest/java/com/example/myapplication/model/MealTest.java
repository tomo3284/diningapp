package com.example.myapplication.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MealTest {

    List<String> allergies;

    @Before
    public void init() {
        allergies = new ArrayList();
        allergies.add("Vegan");
        allergies.add("Vegetarian");
    }

    @Test
    public void getMealName() {
        Meal meal1 = new Meal("coffee", allergies);
        assertTrue(meal1.getMealName().equals("coffee"));

        Meal meal2 = new Meal("", allergies);
        assertTrue(meal2.getMealName().equals(""));
    }

    @Test
    public void getAllergies() {
        Meal meal1 = new Meal("coffee", allergies);
        List<String> testAllergies = meal1.getAllergies();
        assertTrue(testAllergies.equals(allergies));

        allergies.clear();
        Meal meal2 = new Meal("", allergies);
        assertTrue(meal2.getAllergies().equals(allergies));
    }

    @Test
    public void addAllergy() {
        Meal meal1 = new Meal("coffee", allergies);
        meal1.addAllergy("Milk");
        assertTrue(meal1.getAllergies().size() == 3);

        allergies.clear();
        Meal meal2 = new Meal("", allergies);
        meal2.addAllergy("Milk");
        meal2.addAllergy("Egg");
        assertTrue(meal1.getAllergies().size() == 2);
    }

    @Test
    public void removeAllergy() {
        Meal meal1 = new Meal("coffee", allergies);
        meal1.removeAllergy("Vegan");
        assertTrue(meal1.getAllergies().size() == 1);
        assertTrue(meal1.getAllergies().get(0).equals("Vegetarian"));

        allergies.clear();
        Meal meal2 = new Meal("", allergies);
        meal2.removeAllergy("Vegan");
        assertTrue(meal1.getAllergies().size() == 0);
    }
}